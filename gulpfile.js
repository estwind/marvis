'use strict';

const gulp = require('gulp'),
    browserSync  = require('browser-sync'),
    reload = browserSync.reload,
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    cssMin = require('gulp-clean-css'),
    preFixer = require('gulp-autoprefixer');

const path = {

    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/*.js',
        style: 'src/sass/*.scss',
        css: 'src/css/',
        img: 'src/img/*',
        fonts: 'src/fonts/**/*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/*.js',
        style: 'src/sass/*.scss',
        img: 'src/img/*'
    },
    clean: './build'
};

const config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    host: 'localhost',
    port: 3000,
    logPrefix: "frontend"
};

gulp.task('html:build', function () {
    return gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('img:build', function() {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img));
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(preFixer({
            cascade: false
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.build.css))
        .pipe(gulp.dest(path.src.css))
        .pipe(cssMin())
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.reload({stream: true}))
        .pipe(reload({stream: true}));

    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('js:build', function () {
    return gulp.src(path.src.js)
        .pipe(gulp.dest(path.build.js))
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function () {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(reload({stream: true}));
});

gulp.task('build', gulp.parallel('html:build', 'style:build', 'js:build', 'img:build', 'fonts:build'));

gulp.task('browser-sync', function () {
    browserSync(config);
});

gulp.task('watch', function(){
    gulp.watch(path.watch.html, gulp.parallel('html:build'));
    gulp.watch(path.watch.style, gulp.parallel('style:build'));
    gulp.watch(path.watch.js, gulp.parallel('js:build'));
    gulp.watch(path.watch.img, gulp.parallel('img:build'));
});

gulp.task('default', gulp.parallel('build', 'browser-sync', 'watch'));




